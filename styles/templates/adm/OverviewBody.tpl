{include file="overall_header.tpl"}
<center>
	<h1>{$LNG.ow_title}</h1>
	<table width="90%" style="border:2px {if empty($Messages)}colorPositive{else}colorNegative{/if} solid;text-align:center;font-weight:bold;">
		<tr>
			<td class="transparent">{foreach item=Message from=$Messages}
					<span class="colorNegative">{$Message}</span><br>
					{foreachelse}{$LNG.ow_none}{/foreach}
			</td>
		</tr>
	</table>
	<br>
	<table width="80%">
		<tr>
			<th colspan="2">{$LNG.ow_overview}</th>
		</tr>
		<tr>
			<td style="height:50px" colspan="2">{$LNG.ow_welcome_text}</td>
		</tr>
		<tr>
			<th colspan="2">{$LNG.ow_support}</th>
		</tr>
		<tr>
			<td colspan="2"><a href="https://codeberg.org/pr0game/pr0game" target="_blank">Project Homepage</a>
			</td>
		</tr>
		<tr>
			<th colspan="2">{$LNG.ow_updates}</th>
		</tr>
		<tr>
			<td align="center" colspan="2">
				<div id="feed"></div>
			</td>
		</tr>
		<tr>
			<th colspan="2">{$LNG.ow_credits}</th>
		</tr>
		<tr>
			<td colspan="2">
				<table align="center">
					<tr>
						<td class="transparent" colspan="3"><h3>{$LNG.ow_proyect_leader}</h3></td>
					</tr>
					<tr>
						<td class="transparent" colspan="3"><h3><a target="_blank" href="https://github.com/jkroepke" class="colorNegative">Jan</a></h3></td>
					</tr>
				</table>
				<div style="width:100%">
					<div style="float:left;width:50%;min-width:250px;">
						<table>
							<tr>
								<td class="transparent"><p>&nbsp;</p><h3>{$LNG.ow_translator}</h3></td>
							</tr>
							<tr>
								<td class="transparent">
									<table width="250px;">
										<tr>
											<td class="transparent">
												<img src="styles/resource/images/login/flags/us.png" alt="(english)">
											</td>
											<td class="transparent left">
												QwataKayean
											</td>
										</tr>
										<tr>
											<td class="transparent">
												<img src="styles/resource/images/login/flags/pt.png" alt="(portuguese)">
											</td>
											<td class="transparent left">
												QwataKayean
											</td>
										</tr>
										<tr>
											<td class="transparent">
												<img src="styles/resource/images/login/flags/fr.png" alt="(french)">
											</td>
											<td class="transparent left">
												<a href="https://github.com/BigTwoProduction" target="_blank">BigTwoProduction</a>
											</td>
										</tr>
										<tr>
											<td class="transparent">
												<img src="styles/resource/images/login/flags/ru.png" alt="(russian)">
											</td>
											<td class="transparent left">
												InquisitorEA
											</td>
										</tr>
										<tr>
											<td class="transparent">
												<img src="styles/resource/images/login/flags/es.png" alt="(spanish)">
											</td>
											<td class="transparent left">
												Orion
											</td>
										</tr>
										<tr>
											<td class="transparent">
												<img src="styles/resource/images/login/flags/tr.png" alt="(turkish)">
											</td>
											<td class="transparent left">
												romansmac
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</div>
					<div style="float:left;width:50%;min-width:250px;">
						<table>
							<tr>
								<td class="transparent"><p>&nbsp;</p><h3>{$LNG.ow_special_thanks}</h3></td>
							</tr>
							<tr>
								<td class="transparent">
									<table width="250px;">
										<tr>
											<td class="transparent left"><a href="https://github.com/Hilarious001" target="_blank">Hilarious001</a></td>
											<td class="transparent left">Ralf M.</td>
											<td class="transparent left">InquisitorEA</td>
										</tr>
										<tr>
											<td class="transparent left">lucky</td>
											<td class="transparent left">Metusalem</td>
											<td class="transparent left">Meikel</td>
										</tr>
										<tr>
											<td class="transparent left">Phil</td>
											<td class="transparent left">Schnippi</td>
											<td class="transparent left">Vobi</td>
										</tr>
										<tr>
											<td class="transparent left">Sycrog</td>
											<td class="transparent left">Raito</td>
											<td class="transparent left">Chlorel</td>
										</tr>
										<tr>
											<td class="transparent left">e-Zobar</td>
											<td class="transparent left">Flousedid</td>
											<td class="transparent left">jstar</td>
										</tr>
										<tr>
											<td class="transparent left">scrippi</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</div>
				</div>
			</td>
		</tr>
	</table>
</center>
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
	google.load("feeds", "1");
	google.setOnLoadCallback(initialize);
	function initialize() {
		//var feedControl = new google.feeds.FeedControl();
		//feedControl.addFeed("https://github.com/jkroepke/2Moons/commits/master.atom", "");
		//feedControl.addFeed("http://code.google.com/feeds/p/2moons/svnchanges/basic", "");
		//feedControl.draw(document.getElementById("feed"));
		//var feedControl = new google.feeds.FeedControl();
        //feedControl.addFeed("https://www.facebook.com/feeds/page.php?id=129282307106646&format=rss20", "");
        //feedControl.draw(document.getElementById("news"));
	}
</script>
{include file="overall_footer.tpl"}