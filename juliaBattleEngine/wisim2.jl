using HTTP
using Sockets
using JSON
using LinearAlgebra
using Dates
using Statistics

struct Shipinfo
    maxshield::Float64
    attack::Float64
    maxhull::Float64
    function Shipinfo(a,b,c)
        new(a,b,c)
    end
end

mutable struct Stats_current
    amount::Int
    attack::Float64
    hull::Float64
    shield::Float64
    function Stats_current()
        new(0,0,0,0)
    end
    function Stats_current(a,b,c,d)
        new(a,b,c,d)
    end
end

mutable struct Player
    damage::Float64
    totaldamage::Float64
    shieldabsorb::Float64
    totalshieldabsorb::Float64
    id::Int
    destroyed_ships::Dict{Int,Int}
    has_ships::Dict{Int,Stats_current}
    ship_stats::Dict{Int,Shipinfo}
    function Player(id,shipdict)
        new(0.,0.,0.,0.,id,shipdict,Dict{Int,Stats_current}(),Dict{Int,Shipinfo}())
    end
end

mutable struct Ship
    shield::Float64
    hull::Float64
    id::Int
    player::Player
    function Ship(maxshield::Float64, maxhull::Float64,id::Int,player::Player)
        new(maxshield, maxhull,id,player)
    end
end



const ship_destroy = Dict{Int,Dict{Int,Bool}}()

const shipinfos = Dict{Int,Dict{Int,Tuple{Int,Int,Int}}}()

const rapidfiredict = Dict{Int,Dict{Int, Dict{Int, Int}}}()


function load_ship_information()
    println("loading data....")
    empty!(ship_destroy)
    empty!(shipinfos)
    empty!(rapidfiredict)
    file = open(ARGS[2], "r")
    for line in eachline(file)
        universeid,id,attack,shield,hull,destroy,rapidfire=split(line, ";")
        universeid=parse(Int64,universeid)
        id=parse(Int64,id)
        attack=parse(Int64,attack)
        shield=parse(Int64,shield)
        hull=parse(Int64,hull)
        destroy=parse(Bool,destroy)
        rapidfire=strip(rapidfire)
        if haskey(ship_destroy,universeid)==false
            ship_destroy[universeid]=Dict{Int,Bool}()
            shipinfos[universeid]=Dict{Int,Tuple{Int,Int,Int}}()
            rapidfiredict[universeid]=Dict{Int, Dict{Int, Int}}()
        end

        shipinfos[universeid][id]=(attack,shield,hull)
        ship_destroy[universeid][id]=destroy
        rapidfiredict[universeid][id]=Dict{Int, Int}()
        if length(rapidfire)==0
            continue
        end
        rfentries=split(rapidfire,'-')
        for rfe in rfentries
            shipid,amount=split(rfe,',')
            rapidfiredict[universeid][id][parse(Int64,shipid)]=parse(Int64,amount)
        end
    end
end

function calculate_shoot!(attackers::Vector{Ship}, defenders::Vector{Ship},universe::Int)
            for ship in attackers
                shoot_at!(ship,defenders,universe)
            end
            for ship in defenders
                 shoot_at!(ship,attackers,universe)
            end
end

function shoot_at!(ship::Ship,defenders::Vector{Ship},universe::Int)
    shiptarget=rand(defenders)
    if  ship.player.ship_stats[ship.id].attack > shiptarget.shield * 0.01# bounce check
        if shiptarget.hull >0
            #attackshot
            penetration = ship.player.ship_stats[ship.id].attack - shiptarget.shield
            if penetration >= 0
                ship.player.shieldabsorb += shiptarget.shield
                damagedone =min(penetration,shiptarget.hull)
                ship.player.damage +=damagedone
                shiptarget.player.has_ships[shiptarget.id].hull -=damagedone
                shiptarget.shield=0
                shiptarget.hull -=penetration
            else
                ship.player.shieldabsorb +=ship.player.ship_stats[ship.id].attack
                shiptarget.shield -= ship.player.ship_stats[ship.id].attack
            end
            #explosion for ships
            if   ship_destroy[universe][shiptarget.id] && shiptarget.hull < 0.7 * shiptarget.player.ship_stats[shiptarget.id].maxhull && shiptarget.hull >0
                    if rand(0:shiptarget.player.ship_stats[shiptarget.id].maxhull) > shiptarget.hull
                        shiptarget.player.has_ships[shiptarget.id].hull -=shiptarget.hull
                        shiptarget.hull=0
                    end
            end
            if shiptarget.hull <=0
                shiptarget.player.has_ships[shiptarget.id].amount -=1
                shiptarget.player.has_ships[shiptarget.id].shield -=shiptarget.player.ship_stats[shiptarget.id].maxshield
                shiptarget.player.has_ships[shiptarget.id].attack -=shiptarget.player.ship_stats[shiptarget.id].attack
                ship.player.destroyed_ships[shiptarget.id] +=1
            end
        end

        if  haskey(rapidfiredict[universe][ship.id],shiptarget.id)
            if rand(1:rapidfiredict[universe][ship.id][shiptarget.id]) != rapidfiredict[universe][ship.id][shiptarget.id]
                shoot_at!(ship,defenders,universe)
            end
        end

    else

    end
end

function generate_ships(hasships::Dict{Int,Int},weapontech::Int,shieldtech::Int,armortech::Int,player::Player,universe::Int)
    ships= Ship[]
    for (shipid,amount) in sort(collect(pairs(hasships)))
        #println(shipid)
        attack,shield,armor = shipinfos[universe][shipid]
        armor=armor * (1+armortech*0.1)
        shield=shield * (1+shieldtech*0.1)
        attack=attack * (1+weapontech*0.1)
        player.ship_stats[shipid]=Shipinfo(shield,attack,armor)
        player.has_ships[shipid]=Stats_current(amount,attack*amount,armor*amount,shield*amount)
        for i in 1:amount
            push!(ships,Ship(shield,armor,shipid,player))
        end
    end
    return ships
end

function simulate_attack(attackers::Vector{Ship}, defenders::Vector{Ship},attacknames::Vector{Player},defnames::Vector{Player}, rounds::Int,universe::Int)
    totalstart = now()
    shootingtime=0
    calculate_losses=0
    calculate_current=0



    loss_report=Tuple{Dict{String,Int},Dict{String,Int}}[]
    have_report=Tuple{Dict{Int,Dict{Int64, Stats_current}},Dict{Int,Dict{Int64, Stats_current}}}[]
    round=0

    while round < rounds && length(attackers)>0 && length(defenders) >0
        shootstart = now()
        calculate_shoot!(attackers,defenders,universe)
        shootingtime+=Dates.value(now()-shootstart)
        start_current = now()
        attackers=getlosses(attackers)
        calculate_losses+=Dates.value(now()-start_current)
        start_current = now()
        current_ships_attacker=Dict{Int,Dict{Int64, Stats_current}}()
        for player in attacknames
            filter!(x -> x[2].amount != 0, player.has_ships)
            current_ships_attacker[player.id]=deepcopy(player.has_ships)
        end
        calculate_current+=Dates.value(now()-start_current)
        lossda=Dict{String,Int}("shielddamage"=>0,"hulldamage"=>0)
        for player in attacknames
            lossda["shielddamage"] +=floor(player.shieldabsorb)
            lossda["hulldamage"] +=floor(player.damage)
            player.totalshieldabsorb+=player.shieldabsorb
            player.totaldamage +=player.damage
            player.damage=0
            player.shieldabsorb=0
        end
        start_current = now()
        defenders =getlosses(defenders)
        calculate_losses+=Dates.value(now()-start_current)
        start_current = now()
        current_ships_defender=Dict{Int,Dict{Int64, Stats_current}}()
        for player in defnames
            filter!(x -> x[2].amount != 0, player.has_ships)
            current_ships_defender[player.id]=deepcopy(player.has_ships)
        end
        calculate_current+=Dates.value(now()-start_current)
        lossdd=Dict{String,Int}("shielddamage"=>0,"hulldamage"=>0)
        for player in defnames
            lossdd["shielddamage"] +=floor(player.shieldabsorb)
            lossdd["hulldamage"] +=floor(player.damage)
            player.totalshieldabsorb+=player.shieldabsorb
            player.totaldamage +=player.damage
            player.damage=0
            player.shieldabsorb=0
        end
        push!(loss_report,(lossda,lossdd))
        push!(have_report,(current_ships_attacker,current_ships_defender))
        round+=1
    end
    ats=length(attackers)
    dfs=length(defenders)
    totalend = now()
    println("shootingtime:",shootingtime)
    println("calculate losses:",calculate_losses)
    println("calculate current:",calculate_current)
    println("overheadtime:",Dates.value(totalend-totalstart)-shootingtime-calculate_current-calculate_losses)
    if ats == 0 && dfs > 0
          return -1,loss_report,have_report,attacknames,defnames
     end
    if dfs == 0 && ats > 0
        return 1,loss_report,have_report,attacknames,defnames
     end
    return 0,loss_report,have_report,attacknames,defnames
end

function getlosses(ships::Array{Ship})
        for ship in ships
        ship.shield=ship.player.ship_stats[ship.id].maxshield
        end
        return filter((x)-> x.hull >0,ships)
end


function startsim(atdicts::Vector{Tuple{Int,Int,Int,Int,Dict{Int,Int}}},defdicts::Vector{Tuple{Int,Int,Int,Int,Dict{Int,Int}}},universe::Int)
    start = now()
    attackers=Vector{Ship}()
    attackersp=Vector{Player}()
    deffersp=Vector{Player}()
    for (fleetid,wtech,shieldtech,armortech,pd) in atdicts
        deleted_dict=Dict{Int,Int}()
        for (key, value) in shipinfos[universe]
            deleted_dict[key]=0
        end
        push!(attackersp,Player(fleetid,deleted_dict))
        attackers = vcat(attackers,generate_ships(pd,wtech,shieldtech,armortech,attackersp[end],universe))
    end
    dfers=Vector{Ship}()
    for (fleetid,wtech,shieldtech,armortech,pd) in defdicts
        deleted_dict=Dict{Int,Int}()
        for (key, value) in shipinfos[universe]
            deleted_dict[key]=0
        end
        push!(deffersp,Player(fleetid,deleted_dict))
        dfers = vcat(dfers,generate_ships(pd,wtech,shieldtech,armortech,deffersp[end],universe))
    end
    endinit = now()
    startsim = now()
    simres=@inbounds simulate_attack(attackers,dfers,attackersp,deffersp,6,universe)
    endsim = now()
    println("init: ",endinit-start)
    println("sim: ",endsim-startsim)
    return simres
end
precompile(load_ship_information,())
load_ship_information()
precompile(startsim,(Vector{Tuple{Int,Int,Int,Int,Dict{Int,Int}}},Vector{Tuple{Int,Int,Int,Int,Dict{Int,Int}}},Int))
precompile(getlosses,(Vector{Ship},))
precompile(simulate_attack,(Vector{Ship}, Vector{Ship},Vector{Player},Vector{Player},Int,Int))
precompile(generate_ships,(Dict{Int,Int},Int,Int,Int,Player,Int))
precompile(shoot_at!,(Ship,Vector{Ship},Int))
precompile(calculate_shoot!,(Vector{Ship}, Vector{Ship},Int))

#println("started")

#winner,roundlists=startsim([["at1",15,15,15,Dict( 204 =>1000,206=>5000,  207=> 1000,   213=> 5000,214=>1000, 215=> 10000)]],[["at1",15,15,15,Dict( 204 =>1000,206=>5000,  207=> 1000,   213=> 500,214=>1000, 215=> 10000)]])
#println(startsim([["at1",15,15,15,Dict( 214=>250,401 =>5000,405 =>100)]],[["at1",15,15,15,Dict( 204 =>100,206 =>5000,207=>250,215=>1500,213=>100)]]))
#JSON.json(Dict("outcome"=>winner,"losses"=>roundlists))
#@time startsim([["at1",15,15,15,Dict( 214=>1000000)]],[["at1",15,15,15,Dict( 204 =>40_000_000,206 =>15000_000,207=>7000000,215=>5000000,213=>2000000)]])

#exit(0)
#

function update_ships(req::HTTP.Request)
    load_ship_information()
    HTTP.Response(200,"done")

end

function pingalive(req::HTTP.Request)
    HTTP.Response(200,"alive")

end

function postbattle(req::HTTP.Request)
        try
            # Parse the JSON payload from the request
            payload = JSON.parse(String(req.body))
            atrlist=Vector{Tuple{Int,Int,Int,Int,Dict{Int,Int}}}()
            universeid=payload["universe"]
            for atd in payload["attackers"]
                fleetid=pop!(atd, "fleetid")
                wtech=pop!(atd, "wtech")
                stech=pop!(atd, "shieldtech")
                atech=pop!(atd, "armortech")
                shipdict=Dict{Int,Int}()
                for (sid,amount) in atd
                    shipdict[parse(Int64,sid)]=amount
                end
                push!(atrlist,(fleetid,wtech,stech,atech,shipdict))
            end
            deflist=Vector{Tuple{Int,Int,Int,Int,Dict{Int,Int}}}()
            for atd in payload["defenders"]
                fleetid=pop!(atd, "fleetid")
                wtech=pop!(atd, "wtech")
                stech=pop!(atd, "shieldtech")
                atech=pop!(atd, "armortech")
                shipdict=Dict{Int,Int}()
                for (sid,amount) in atd
                    shipdict[parse(Int64,sid)]=amount
                end
                push!(deflist,(fleetid,wtech,stech,atech,shipdict))
            end
            winner,loss_report,have_report,attack_players,def_players=startsim(atrlist,deflist,universeid)
            destroyedstats_attacker=Dict{Int,Dict{Int,Int}}()
            for player in attack_players
                destroyedstats_attacker[player.id]=Dict{Int,Int}()
                destroyedstats_attacker[player.id][-1]=floor(player.totalshieldabsorb)
                destroyedstats_attacker[player.id][-2]=floor(player.totaldamage)
                for (shipid,shipamount) in player.destroyed_ships
                    destroyedstats_attacker[player.id][shipid]=shipamount
                end
            end
            destroyedstats_defender=Dict{Int,Dict{Int,Int}}()
            for player in def_players
                destroyedstats_defender[player.id]=Dict{Int,Int}()
                destroyedstats_defender[player.id][-1]=floor(player.totalshieldabsorb)
                destroyedstats_defender[player.id][-2]=floor(player.totaldamage)
                for (shipid,shipamount) in player.destroyed_ships
                    destroyedstats_defender[player.id][shipid]=shipamount
                end
            end
            #TODO destroyed dict
            # Respond with a success message
            return HTTP.Response(200,JSON.json(Dict("outcome"=>winner,"round_lost"=>loss_report,"round_have"=>have_report,"destroyed_attacker"=>destroyedstats_attacker,"destroyed_defender"=>destroyedstats_defender)))
        catch err
            println(err)
            # Respond with an error message if JSON parsing fails
            return HTTP.Response(400,  "{\"error\": \"Invalid JSON payload\"}")
        end

end


function gethtml(req::HTTP.Request)
    # Extract the requested path from the URL
    path = req.target[2:end]
    # Check if the requested path is a file and exists
    if isfile(path)
        # Read the contents of the file
        content = String(read(path))

        # Return a response with the file content and appropriate headers
        return HTTP.Response(200, content)
    else
        # Return a 404 Not Found response if the file doesn't exist
        return HTTP.Response(404, "File not found")
    end
end

function kill(req::HTTP.Request)
    exit(0)
end


function calculate_stats(data::Vector{Vector{Dict{Int, Int}}}, index::Int, key::Int)
    values = Int[]
    # Iterate over each list in data
    for sublist in data
        # Check if the index is within the range of sublist length
        if index <= length(sublist)
            # Extract value from the dictionary at the specified index and key
            value = get(sublist[index], key, nothing)
            if value !== nothing  # Check if the key exists in the dictionary
                push!(values, value)
            end
        end
    end

        min_value = minimum(values)
        max_value = maximum(values)
        mean_value = mean(values)
        variance_value = sqrt(var(values))
        return Tuple([min_value, max_value, mean_value, variance_value])

end


function postbattlesim(req::HTTP.Request)
        #try
            # Parse the JSON payload from the request
            payload = JSON.parse(String(req.body))

            # Process the payload as needed
            #println("Received JSON payload: ", payload)
            atrlist=Vector{Tuple{Int,Int,Int,Int,Dict{Int,Int}}}()
            universeid=payload["universe"]
            for atd in payload["attackers"]
                fleetid=pop!(atd, "fleetid")
                wtech=pop!(atd, "wtech")
                stech=pop!(atd, "shieldtech")
                atech=pop!(atd, "armortech")
            shipdict=Dict{Int,Int}()
            for (sid,amount) in atd
            shipdict[parse(Int64,sid)]=amount
            end
            push!(atrlist,(fleetid,wtech,stech,atech,shipdict))
            end
            deflist=Vector{Tuple{Int,Int,Int,Int,Dict{Int,Int}}}()
            for atd in payload["defenders"]
                fleetid=pop!(atd, "fleetid")
                wtech=pop!(atd, "wtech")
                stech=pop!(atd, "shieldtech")
                atech=pop!(atd, "armortech")
                shipdict=Dict{Int,Int}()
                for (sid,amount) in atd
                    shipdict[parse(Int64,sid)]=amount
                end
                push!(deflist,(fleetid,wtech,stech,atech,shipdict))
            end
            rounds=payload["rounds"]
            winlist=[]
            total_losses_att=Vector{Vector{Dict{Int,Int}}}()
            total_losses_def=Vector{Vector{Dict{Int,Int}}}()
            for i in 1:rounds
            #println(i)
                winner,loss_report,have_report,attack_players,def_players=startsim(deepcopy(atrlist),deepcopy(deflist),universeid)
                push!(winlist,winner)
                losses_att=Vector{Dict{Int,Int}}()
                losses_def=Vector{Dict{Int,Int}}()
                for attackerid in eachindex(attack_players)
                    playerid=atrlist[attackerid][1]
                    losses=Dict{Int,Int}()
                    for (shipid,amount) in atrlist[attackerid][5]
                        if haskey(attack_players[attackerid].has_ships,shipid)
                            losses[shipid]=amount - attack_players[attackerid].has_ships[shipid].amount
                        else
                            losses[shipid]=amount
                        end
                    end
                    push!(losses_att,losses)
                end
                for deferid in eachindex(def_players)
                    playerid=deflist[deferid][1]
                    losses=Dict{Int,Int}()
                    for (shipid,amount) in deflist[deferid][5]
                        if haskey(def_players[deferid].has_ships,shipid)
                            losses[shipid]=amount - def_players[deferid].has_ships[shipid].amount
                        else
                            losses[shipid]=amount
                        end
                    end
                    push!(losses_def,losses)
                end
                push!(total_losses_att,losses_att)
                push!(total_losses_def,losses_def)
            end
            atterstats=Dict{Int,Dict{Int,Tuple{Int,Int,Float64,Float64}}}()
            defferstats=Dict{Int,Dict{Int,Tuple{Int,Int,Float64,Float64}}}()
            for playeridx in eachindex(total_losses_att[1])
                atterstats[atrlist[playeridx][1]]=Dict{Int,Tuple{Int,Int,Float64,Float64}}()
                for (shipid,amount) in total_losses_att[1][playeridx]
                atterstats[atrlist[playeridx][1]][shipid]=calculate_stats(total_losses_att, playeridx, shipid)
                end
            end

            for playeridx in eachindex(total_losses_def[1])
                defferstats[atrlist[playeridx][1]]=Dict{Int,Tuple{Int,Int,Float64,Float64}}()
                for (shipid,amount) in total_losses_def[1][playeridx]
                defferstats[atrlist[playeridx][1]][shipid]=calculate_stats(total_losses_def, playeridx, shipid)
                end
            end
            # Respond with a success message
            return HTTP.Response(200,JSON.json(Dict("outcomes"=>winlist,"atterstats"=>atterstats,"defferstats"=>defferstats)))
        #=catch err
            # Respond with an error message if JSON parsing fails
            println("serror ",err)
            return HTTP.Response(400,  "{\"error\": \"Invalid JSON payload\"}")
        end=#
end


function main()
    router = HTTP.Router()
    HTTP.register!(router, "POST", "/battlesim", postbattle)
    HTTP.register!(router, "POST", "/battlesimmulti", postbattlesim)
    HTTP.register!(router, "GET", "/updateships", update_ships)
    HTTP.register!(router, "GET", "/ping", pingalive)
    HTTP.register!(router, "GET", "/kill", kill)
    HTTP.register!(router, "GET", "/*", gethtml)
    server = HTTP.serve!(router, Sockets.localhost,parse(Int64, ARGS[1]))
    wait(server)
end
function julia_main()::Cint
 main()
  return 0 # if things finished successfully
end

precompile(postbattle,(HTTP.Request,))
precompile(gethtml,(HTTP.Request,))

julia_main()